import { Button, Card } from 'react-bootstrap';
import { useShoppingCart } from '../context/ShoppinCartContext';
import Currency from '../utilities/Currency';

type StoreItemsProps = {
  id: number;
  name: string;
  price: number;
  imgUrl: string;
};

export function StoreItem({ id, name, price, imgUrl }: StoreItemsProps) {
  const {
    decreaseCartQuantity,
    increaseCartQuantity,
    getItemQuantity,
    removeFromCart,
  } = useShoppingCart();

  const quantity = getItemQuantity(id);

  return (
    <>
      <Card>
        <Card.Img
          variant="top"
          src={imgUrl}
          height="300px"
          style={{ objectFit: 'cover' }}
        />
        <Card.Body className="d-flex flex-column">
          <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
            <span className="fs-2">{name}</span>
            <span className="ms-2 text-muted">{Currency.format(price)}</span>
          </Card.Title>

          {quantity === 0 ? (
            <Button
              className=""
              type="button"
              onClick={() => increaseCartQuantity(id)}
            >
              + Add to cart
            </Button>
          ) : (
            <>
              <div className="d-flex mt-2 mb-4 align-items-center justify-content-center gap-4">
                <Button
                  className="px-4"
                  type="button"
                  onClick={() => decreaseCartQuantity(id)}
                >
                  -
                </Button>
                <div className="fs-4 fw-bol">{quantity}</div>
                <Button
                  className="px-4"
                  type="button"
                  onClick={() => increaseCartQuantity(id)}
                >
                  +
                </Button>
              </div>

              <Button
                className="btn-danger"
                type="button"
                onClick={() => removeFromCart(id)}
              >
                Remove
              </Button>
            </>
          )}
        </Card.Body>
      </Card>
    </>
  );
}
