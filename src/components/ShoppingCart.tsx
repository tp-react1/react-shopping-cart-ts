import React from 'react';
import { Offcanvas, Stack } from 'react-bootstrap';
import { useShoppingCart } from '../context/ShoppinCartContext';
import Currency from '../utilities/Currency';
import CartItem from './CartItem';
import storeItems from '../data/items.json';

type ShoppingCartProps = {
  isOpen: boolean;
};

export const ShoppingCart = ({ isOpen }: ShoppingCartProps) => {
  const { closeCart, cartItems, cartQuantity, getTotalAmount } =
    useShoppingCart();
  let total = 0;

  return (
    <Offcanvas show={isOpen} onHide={closeCart} placement="end">
      <Offcanvas.Header closeButton>
        <Offcanvas.Title>Cart content</Offcanvas.Title>
      </Offcanvas.Header>
      <Offcanvas.Body>
        <hr />
        <div className="total-amount">
          <div>Total</div>
          <div className="price">
            {Currency.format(getTotalAmount(storeItems))}
          </div>
        </div>

        <hr />

        {cartQuantity == 0 && (
          <div className="alert alert-primary">your cart is empty...</div>
        )}

        <Stack gap={3}>
          {cartItems.map((item) => {
            if (item.quantity > 0) return <CartItem key={item.id} {...item} />;
            else return null;
          })}
        </Stack>
      </Offcanvas.Body>
    </Offcanvas>
  );
};
