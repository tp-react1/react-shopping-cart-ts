export default class Currency {
  static CURRENCY_FORMATTER = new Intl.NumberFormat(undefined, {
    currency: 'USD',
    style: 'currency',
  });

  static format = (number: number) => {
    return Currency.CURRENCY_FORMATTER.format(number);
  };
}
