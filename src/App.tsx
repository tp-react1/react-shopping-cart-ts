import { useState } from 'react';

import { Routes, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { Home } from './pages/Home';
import { Store } from './pages/Store';
import { About } from './pages/About';
import { Navbar } from './components/Navbar';
import { ShoppingCartProvider } from './context/ShoppinCartContext';
import './App.css';
import { name, version } from '../package.json';

function App() {
  return (
    <ShoppingCartProvider>
      <div className="App">
        <Navbar />
        <Container className="mb-4">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/store" element={<Store />} />
            <Route path="/about" element={<About />} />
          </Routes>
        </Container>
        <footer>v{version}&copy;FCH</footer>
      </div>
    </ShoppingCartProvider>
  );
}

export default App;
